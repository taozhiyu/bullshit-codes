    @ApiOperation(value = "分页列表", notes = "分页列表")
    @PreAuthorize("@ss.hasPermi('breed:DiffSizeSlaughterVolume:list')")
    @GetMapping("/pageList")
    public R<Page<BreedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoDTO>> pageList(Page page, BreedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoDTO breedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoDTO) {
        return R.ok(BreedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoConvert.pageConvert(
                    breedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoService.page(page, new QueryWrapper<>(
                                BreedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoConvert.dtoToDo(breedSlaughterhouseAboveDesignatedSizeSlaughterVolumeInfoDTO)))));
    }