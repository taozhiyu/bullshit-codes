
```
比如 有一个Users表，字段包含(id:integer r_id:integer)
                A
            |       |
            B       C
          |   |
          D   E
          |
          F

查询一个用户下的团队用户，通过A 查询B、C、D、E、F的用户ID
之前写了一个轮询的方法查询:
def self.polling(r_id, user_ids=[])
  ids = user.where(r_user_id: r_id).ids
  user_ids += ids
  User.polling(ids, user_ids)
end
# 调用： User.polling(A.id)
```
可以类似修改如下：
```
使用以下sql 速度很快
def polling
  # 动态传值，须谨慎防止SQL注入
  # 动态传值，须谨慎防止SQL注入
  # 动态传值，须谨慎防止SQL注入
  sql = "WITH RECURSIVE recommended_users(r_id, user_id) AS (
    SELECT r_id, user_id FROM users WHERE user_id = #{self.id}
    UNION ALL
    SELECT users.r_id, recommended_users.user_id FROM recommended_users
    INNER JOIN users ON users.user_id = recommended_users.r_id
  )
  SELECT r_id FROM recommended_users;"
  # 执行SQL
  ActiveRecord::Base.connection.execute sql
end

```