﻿using System;

namespace Grab
{
    public class LogUtils
    {
        /// <summary>
        /// 本地log
        /// </summary>
        /// <param name="log"></param>
        /// <param name="type"></param>
        public static void WriteLog(String log, String type = "info")
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "/log/";
                DirectoryInfo info = new DirectoryInfo(path);
                if (!info.Exists) {
                    info.Create();
                }
                StreamWriter sw;
                FileInfo fi = new FileInfo(path + type + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log");
                if (!fi.Exists) {
                    sw = fi.CreateText();
                } else {
                    sw = File.AppendText(fi.FullName);
                }
                sw.WriteLine("================" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "================");
                sw.WriteLine(log);
                sw.Close();
                sw.Dispose();
            }
            catch (Exception e)
            {
            }
        }
      
    }
}
