import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Resource
    private UserDAO userDao;
    @Autowired
    private BankSignService bankSignService;
    
    //TODO 巨坑：一个5年经验的java开发写的，看到这个异步执行，我笑了。
    //TODO 他的本意是想在入库用户信息后，异步调银行签约卡户逻辑，但@Async、@Transction等Spring注解的方法是需要注入Spring代理对象调用才能生效，直接调是不生效的
    
    /**
     * 用户注册服务，保存用户信息后，异步调银行签约开户
     */
    public Result<Void> register(UserDTO userDto){
        userDao.save(userDto);
        asynBankSign(userDto);
        return Result.success()
    }
    
    /**
     * Spring的异步框架异步执行银行签约逻辑
     */
    @Async
    private  void asynBankSign(UserDTO userDto) {
        // TODO do something
    }
    

}