/**
 * 
 * @Description: spring boot 的redis工具类
 */
@SuppressWarnings("unchecked")
@Component
public class RedisUtil {
    // 引入了两个template完全多余
    @SuppressWarnings("rawtypes")
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
 
    /**
     * 批量删除对应的value
     * 
     * @param keys
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            /*
            template完全支持批量删除，底下的方法就用到了，这里偏要一个一个删
             */
            remove(key);
        }
    }
 
    /**
     * 批量删除key
     * 
     * @param pattern
     */
    public void removePattern(final String pattern) {
        /*
        暂且不提keys命令大量数据下不可用，ide都提示了可能存在空指针
         */
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0)
            redisTemplate.delete(keys);
    }
 
    /**
     * 删除对应的value
     * 
     * @param key
     */
    public void remove(final String key) {
        /**
         * 这里就完全想不通了，删除命令是有返回值的，删除一个不存在的key没有任何问题
         */
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }
 
    /**
     * 判断缓存中是否有对应的value
     * 
     * @param key
     * @return
     */
    public boolean exists(final String key) {
        /**
         * hasKey返回的是包装类，这里唯一能想到的是给方法一个别名
         */
        return redisTemplate.hasKey(key);
    }
 
    /**
     * 读取缓存
     * 
     * @param key
     * @return
     */
    public String get(final String key) {
        /**
         * 啊。。。这。。。。上面的StringRedisTemplate是拿来干嘛的？？而且怎么可能每次都new一个
         */
        Object result = null;
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        result = operations.get(key);
        if (result == null) {
            return null;
        }
        return result.toString();
    }
 
    /**
     * 写入缓存
     * 
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value) {
        // 这里只是写得啰嗦了一点，倒不是什么大问题
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
 
    /**
     * 写入缓存
     * 
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value, Long expireTime) {
        // 但凡多按一个逗号，就知道一个命令就能搞定
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
 
    public boolean hmset(String key, Map<String, String> value) {
        boolean result = false;
        try {
            redisTemplate.opsForHash().putAll(key, value);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
 
    public Map<String, String> hmget(String key) {
        Map<String, String> result = null;
        try {
            result = redisTemplate.opsForHash().entries(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void setStr(final String key, final String value, final long expireTime) {
        // 恭喜啊，又学到了新知识，学习速度不可想象啊。就是问一句工资是按代码行数算的嘛？
        stringRedisTemplate.opsForValue().set(key, value, expireTime, TimeUnit.SECONDS);
    }

    public String getStr(final String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    public void delStr(final String key){
        // 对不起，我已经没话说了,只是希望早日治好失忆症
        if(redisTemplate.hasKey(key)){
            redisTemplate.delete(key);
        }
    }

}