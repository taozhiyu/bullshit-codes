package com.bimcloud.bim.work.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bimcloud.bim.admin.api.entity.SysDictItem;
import com.bimcloud.bim.admin.api.feign.RemoteDictService;
import com.bimcloud.bim.common.core.util.R;
import com.bimcloud.bim.work.Enum.DateTypeEnums;
import com.bimcloud.bim.work.Enum.NumericEnum;
import com.bimcloud.bim.work.entity.BaseArea;
import com.bimcloud.bim.work.entity.Report;
import com.bimcloud.bim.work.entity.vo.DeviceFaultVo;
import com.bimcloud.bim.work.mapper.BaseAreaMapper;
import com.bimcloud.bim.work.mapper.ReportMapper;
import com.bimcloud.bim.work.service.ReportService;
import com.bimcloud.bim.work.util.CommonUtil;
import com.bimcloud.bim.work.util.ConstantUtil;
import com.bimcloud.bim.work.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements ReportService {

	private final BaseAreaMapper baseAreaMapper;

	private final RemoteDictService remoteDictService;

	/**
	 * 工单来源与故障分布
	 *
	 * @return
	 */
	@Override
	public Report getGdAndFaultCount(Report report) {
		//spliceTime(report);

		return baseMapper.getGdAndFaultCount(report);
	}

	/**
	 * 统计每月工单总数与处理情况
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report getTaskInfoTotal(Report report) {
		Report report1 = new Report();
		int taskStatus1 = 0;
		int taskStatus3 = 0;
		int taskStatus7 = 0;
		//时间参数查询
		List<Report> reportList = baseMapper.getTaskInfoTotal(report);
		if (CollectionUtil.isNotEmpty(reportList)) {
			for (Report info : reportList) {
				taskStatus1 = taskStatus1 + info.getTaskStatus1();
				taskStatus3 = taskStatus3 + info.getTaskStatus3();
				taskStatus7 = taskStatus7 + info.getTaskStatus7();
			}
		}
		//处理查询参数
//		int startMonth = getMonth(report);
//		getMonthTask(report, report1, startMonth);
		report1.setTaskStatus1(taskStatus1);
		report1.setTaskStatus3(taskStatus3);
		report1.setTaskStatus7(taskStatus7);
		return report1;
	}


	/**
	 * 每日工单数
	 *
	 * @param report
	 * @return
	 */
	@Override
	public List<Report> getTaskInfoTotalDay(Report report) {


		String startTime = CommonUtil.isNull(report.getStartTime());
		String endTime = CommonUtil.isNull(report.getEndTime());

		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String curTime = sdf.format(d);
		if (startTime.equals("")){
			startTime = curTime;
		}
		if (endTime.equals("")){
			endTime = curTime;
		}

		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		try {
			c1.setTime(sdf.parse(startTime));
			c2.setTime(sdf.parse(endTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}


		List<LocalDate> localDateList = getsAllDatesInTheDateRange(
				LocalDate.of(c1.get(Calendar.YEAR), c1.get(Calendar.MARCH) + 1, c1.get(Calendar.DAY_OF_MONTH)),
				LocalDate.of(c2.get(Calendar.YEAR), c2.get(Calendar.MARCH) + 1, c2.get(Calendar.DAY_OF_MONTH))
		);


		List<Report> reportListRtn = new ArrayList<Report>();
		if (CollectionUtil.isNotEmpty(localDateList)) {
			for (LocalDate localDate : localDateList) {
				// System.out.println("当前时间---："+localDate.toString());
				Report tempBO = new Report();
				tempBO.setTjTime(localDate.toString());
				tempBO.setTaskTotal(0);
				reportListRtn.add(tempBO);
			}
		}

		//时间参数查询
		List<Report> reportList = baseMapper.getTaskInfoTotalDay(report);
		Map<String,Integer> map = new HashMap<String,Integer>();
		if (CollectionUtil.isNotEmpty(reportList)) {
			for (Report info : reportList) {
				map.put(info.getTjTime(),info.getTaskTotal());
				//System.out.println("当前时间---："+info.getTjTime() + "--"+info.getTaskTotal());
			}
		}

		for (Report info : reportListRtn) {
			if (map.containsKey(info.getTjTime())){
				info.setTaskTotal(map.get(info.getTjTime()));
			}
		}


		return reportListRtn;
	}

	/**
	 * 统计故障数(年月日)
	 * @param report
	 * @return
	 */
	@Override
	public Report countTaskInfoTotal(Report report) {
		List<Report> taskInfoTotalDayList = this.baseMapper.getTaskInfoTotalDay(report);
		//计算时间段
		List<String> quantumDateList = DateTimeUtil.getQuantumDate(report.getType(), report.getStartTime(), report.getEndTime());
		//按时间分组
		Map<String, List<Report>> reportMap = taskInfoTotalDayList.stream().map(e->{
			Report dealReport = new Report();
			String tjTime ="";
			if (DateTypeEnums.TYPEYEAR.getCode().equals(report.getType())){
				tjTime = e.getTjTime().substring(0,4);
			}else if ((DateTypeEnums.TYPEMONTH.getCode().equals(report.getType()))){
				tjTime =e.getTjTime().substring(0,7);
			}else{
				tjTime = e.getTjTime().substring(0,10);
			}
			dealReport.setTjTime(tjTime);
			dealReport.setTaskStatus1(e.getTaskStatus1());
			dealReport.setTaskStatus3(e.getTaskStatus3());
			dealReport.setTaskStatus7(e.getTaskStatus7());
            return dealReport;
		}).collect(Collectors.groupingBy(Report::getTjTime));
		List<Report> finalList = quantumDateList.stream().map(e -> {
			Report filterReport = new Report();
			//taskStatus1
			int taskStatus1Total = 0;
			//taskStatus3
			int taskStatus3Total = 0;
			//taskStatus1
			int taskStatus7Total = 0;
			filterReport.setTjTime(e);
			List<Report> reportList = reportMap.get(e);
			if (CollectionUtil.isNotEmpty(reportList)) {
				//taskStatus1
				taskStatus1Total = reportList.stream().mapToInt(Report::getTaskStatus1).sum();
				//taskStatus3
				taskStatus3Total = reportList.stream().mapToInt(Report::getTaskStatus3).sum();
				//taskStatus1
				taskStatus7Total = reportList.stream().mapToInt(Report::getTaskStatus7).sum();
			}
			//总任务数
			//int taskTotalNum = taskStatus1Total + taskStatus3Total + taskStatus7Total;
			//filterReport.setTaskInfoTotalNum(taskTotalNum);
			filterReport.setTaskStatus1(taskStatus1Total);
			filterReport.setTaskStatus3(taskStatus3Total);
			filterReport.setTaskStatus7(taskStatus7Total);
			return filterReport;
		}).collect(Collectors.toList());
		Report dealReport = new Report();
		int taskStatus1Total = finalList.stream().mapToInt(Report::getTaskStatus1).sum();
		//taskStatus3
		int taskStatus3Total = finalList.stream().mapToInt(Report::getTaskStatus3).sum();
		//taskStatus1
		int taskStatus7Total = finalList.stream().mapToInt(Report::getTaskStatus7).sum();
		dealReport.setTaskStatus1(taskStatus1Total);
		dealReport.setTaskStatus3(taskStatus3Total);
		dealReport.setTaskStatus7(taskStatus7Total);
		return dealReport;
	}

	/**
	 * 巡视数量对比
	 * @param report
	 * @return
	 */
	@Override
	public Report countTourStatus(Report report) {
		//查询巡视任务总数,与不同状态的数量
		List<Report> reportList = baseMapper.selectTourStatus(report);
		Report dealReport = new Report();
		if (CollectionUtil.isNotEmpty(reportList)){
			int tourStatus0 = reportList.stream().mapToInt(Report::getTourStatus0).sum();
			int tourStatus1 = reportList.stream().mapToInt(Report::getTourStatus1).sum();
			int tourStatus2 = reportList.stream().mapToInt(Report::getTourStatus2).sum();
			dealReport.setTourStatus1(tourStatus1);
			dealReport.setTourStatus0(tourStatus0);
			dealReport.setTourStatus2(tourStatus2);
		}
		Report patrolReport = baseMapper.selectPatrolType(report);
		if (null!=patrolReport) {
			dealReport.setPatrolType0(patrolReport.getPatrolType0());
			dealReport.setPatrolType1(patrolReport.getPatrolType1());
			dealReport.setPatrolType2(patrolReport.getPatrolType2());
		}
		return dealReport;
	}
	/**
	 * 统计年月日的数据TourTotal
	 * @param report
	 * @return
	 */
	@Override
	public List<Report> countYearMonthDayTourTotal(Report report) {
		 List<Report> reportList = baseMapper.selectTourStatus(report);
		 //统计结果按日期分组
		 Map<String, List<Report>> reportMap = reportList.stream().map(e -> {
			Report dealReport = new Report();
			String tjTime = "";
			if (DateTypeEnums.TYPEYEAR.getCode().equals(report.getType())) {
				tjTime = e.getTjTime().substring(0, 4);
			} else if ((DateTypeEnums.TYPEMONTH.getCode().equals(report.getType()))) {
				tjTime = e.getTjTime().substring(0, 7);
			} else {
				tjTime = e.getTjTime().substring(0, 10);
			}
			dealReport.setTjTime(tjTime);
			dealReport.setTourStatus0(e.getTourStatus0());
			dealReport.setTourStatus1(e.getTourStatus1());
			dealReport.setTourStatus2(e.getTourStatus2());
			return dealReport;
		}).collect(Collectors.groupingBy(Report::getTjTime));
        //根据日期统计每个状态的数量
		return DateTimeUtil.getQuantumDate(report.getType(),report.getStartTime(),report.getEndTime())
				.stream().map(n->{
					int tourStatus0 = 0;
					int tourStatus1 =0;
					int tourStatus2 =0;
					Report filterReport = new Report();
					filterReport.setTjTime(n);
					List<Report> reports = reportMap.get(n);
					if (CollectionUtil.isNotEmpty(reports)){
						 tourStatus0 = reports.stream().mapToInt(Report::getTourStatus0).sum();
						 tourStatus1 = reports.stream().mapToInt(Report::getTourStatus1).sum();
						 tourStatus2 = reports.stream().mapToInt(Report::getTourStatus2).sum();
					}
					filterReport.setTourStatus0(tourStatus0);
					filterReport.setTourStatus1(tourStatus1);
					filterReport.setTourStatus2(tourStatus2);
					return filterReport;
				}).collect(Collectors.toList());
	}

	/**
	 * 工单数量对比
	 * @param report
	 * @return
	 */
	@Override
	public List<Report> countMonthTaskTotal(Report report) {
		List<Report> reportList = baseMapper.getTaskInfoTotal(report);
		if (CollectionUtil.isEmpty(reportList)){
			 return new ArrayList<>();
		}
		//切分日期
		Map<String, List<Report>> reportMap = reportList.stream().map(taskInfo -> {
			Report filterReport = new Report();
			String dateTime[] = taskInfo.getTjTime().split("-");
			StringBuffer stringBuffer = new StringBuffer();
			if (DateTypeEnums.TYPEYEAR.getCode().equals(report.getType())) {
				filterReport.setTjTime(stringBuffer.append(dateTime[0]).toString());
			} else if (DateTypeEnums.TYPEMONTH.getCode().equals(report.getType())) {
				filterReport.setTjTime(stringBuffer.append(dateTime[0]).append(dateTime[1]).toString());
			} else {
				filterReport.setTjTime(stringBuffer.append(dateTime[0]).append(dateTime[1]).append(dateTime[2]).toString());
			}
			filterReport.setTaskStatus1(taskInfo.getTaskStatus1());
			filterReport.setTaskStatus3(taskInfo.getTaskStatus3());
			filterReport.setTaskStatus7(taskInfo.getTaskStatus7());
			return filterReport;
		}).collect(Collectors.groupingBy(Report::getTjTime));
		//筛选存在的日期
	   return DateTimeUtil.getQuantumDate(report.getType(), report.getStartTime(), report.getEndTime())
			   .stream().map(quantumDate->{
				   Report dealReport = new Report();
				   List<Report> reports = reportMap.get(quantumDate);
				   int taskStatus1Total =0;
				   int taskStatus3Total =0;
				   int taskStatus7Total =0;
				   if (CollectionUtil.isNotEmpty(reports)){
					   taskStatus1Total = reports.stream().mapToInt(Report::getTaskStatus1).sum();
					   taskStatus3Total = reports.stream().mapToInt(Report::getTaskStatus3).sum();
					   taskStatus7Total = reports.stream().mapToInt(Report::getTaskStatus7).sum();
				   }
				   int totalTaskNum = taskStatus1Total+taskStatus3Total+taskStatus7Total;
				   dealReport.setTaskInfoTotalNum(totalTaskNum);
				   dealReport.setTjTime(quantumDate);
				   return dealReport;
			   }).collect(Collectors.toList());
	}

	public static List<LocalDate> getsAllDatesInTheDateRange(LocalDate startDate, LocalDate endDate) {
		List<LocalDate> localDateList = new ArrayList<>();
		// 开始时间必须小于结束时间
		if (startDate.isAfter(endDate)) {
			return null;
		}
		while (startDate.isBefore(endDate)) {
			localDateList.add(startDate);
			startDate = startDate.plusDays(1);
		}
		localDateList.add(endDate);
		return localDateList;
	}

	@Override
	public Report getMonthTaskTotal(Report report) {
		Report report1 = new Report();
		//处理查询参数
		int startMonth = handleParam(report);
		getMonthTask(report, report1, startMonth);
		return report1;
	}

	private void getMonthTask(Report report, Report report1, int startMonth) {
		DateTime endDate = DateUtil.parse(report.getEndTime(), ConstantUtil.SDFDATE);
		DateTime startDate = DateUtil.parse(report.getStartTime(), ConstantUtil.SDFDATE);
		Long count = DateUtil.betweenMonth(endDate, startDate, false) + 1;
		int betweenMonth = count.intValue();
		int endMonth = DateUtil.month(endDate) + 1;

		//初始化数组
		int[] monthTotal = new int[betweenMonth];
		String[] months = new String[betweenMonth];
		for (int i = 0; i < betweenMonth; i++) {
			monthTotal[i] = 0;
			int month = DateUtil.offsetMonth(startDate, i + 1).getField(DateField.MONTH);
			if (month == 0) {
				month = 12;
			}
			months[i] = month + "月";
		}
		List<Report> reportList2 = baseMapper.getTaskInfoTotal(report);
		//一个月最多5周
		if (CollectionUtil.isNotEmpty(reportList2)) {
			for (int i = 0; i < reportList2.size(); i++) {
				Report info = reportList2.get(i);
				int infoMonth = info.getInfoMonth();
				int index = 0;
				if (infoMonth <= endMonth) {
					index = (betweenMonth - 1) + infoMonth - endMonth;
				} else {
					//如果大于结束月，说明是上一年
					index = infoMonth - startMonth;
				}
				monthTotal[index] = info.getTaskTotal();
			}
		}
		report1.setMonths(months);
		report1.setMonthTotal(monthTotal);
	}

	//拼接时间后缀
	private void spliceTime(Report report) {
		if (StrUtil.isNotBlank(report.getEndTime())) {
			report.setEndTime(report.getEndTime() + " 23:59:59");
		}

	}

	private int getMonth(Report report) {
		Date date = new Date();
		//前六个月
		DateTime dateTime = DateUtil.offsetMonth(date, -5);
		DateTime dateTime1 = DateUtil.endOfMonth(date);
		report.setEndTime(DateUtil.format(dateTime1, ConstantUtil.SDFDATETIME));
		//前半年，所以需要减5
		int month = DateUtil.month(dateTime) + 1;
		report.setStartTime(DateUtil.format(DateUtil.beginOfMonth(dateTime), ConstantUtil.SDFDATETIME));
		return month;
	}

	/**
	 * 处理参数
	 *
	 * @param report
	 * @return
	 */
	private int handleParam(Report report) {
		Date date = new Date();
		if (StrUtil.isNotEmpty(report.getStartTime())) {
			date = DateUtil.parse(
					report.getStartTime(), ConstantUtil.SDFDATE);
		}
		//前六个月
		DateTime dateTime = null;
		if (StrUtil.isBlank(report.getEndTime())) {
			dateTime = DateUtil.offsetMonth(date, -5);
			DateTime dateTime1 = DateUtil.endOfMonth(date);
			report.setEndTime(DateUtil.format(dateTime1, ConstantUtil.SDFDATETIME));
		} else {
			dateTime = new DateTime(date.getTime());
			report.setEndTime(report.getEndTime() + " 23:59:59");
		}
		//前半年，所以需要减5
		int month = DateUtil.month(dateTime) + 1;
		report.setStartTime(DateUtil.format(DateUtil.beginOfMonth(dateTime), ConstantUtil.SDFDATETIME));
		return month;
	}

	/**
	 * 查询车辆相关费用以及出入库金额
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report getAmount(Report report) {
		//spliceTime(report);
		//查询车辆
		Report report1 = baseMapper.getCarAmount(report);
		//查询出库入库耗材的金额
		List<BigDecimal> rkAnCkAmount = baseMapper.getRkAndCkAmount(report);
		report1.setRkAmount(rkAnCkAmount.get(0));
		report1.setCkAmount(rkAnCkAmount.get(1));

		return report1;
	}

	/**
	 * 巡视任务状态统计
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report getTourStatus(Report report) {

		if (report.getStartTime() == null) {
			report.setStartTime(DateUtil.format(DateUtil.beginOfYear(new Date()), ConstantUtil.SDFDATETIME));
		}


		spliceTime(report);
		Report report1 = new Report();
		int tourStatus0 = 0;
		int tourStatus1 = 0;
		int tourStatus2 = 0;
		List<Report> reportList0 = baseMapper.selectTourStatus(report);
		if (CollectionUtil.isNotEmpty(reportList0)) {
			for (Report report2 : reportList0) {
				tourStatus0 = tourStatus0 + report2.getTourStatus0();
				tourStatus1 = tourStatus1 + report2.getTourStatus1();
				tourStatus2 = tourStatus2 + report2.getTourStatus2();
			}
		} else {
			return report1;
		}

		report1.setTourStatus0(tourStatus0);
		report1.setTourStatus1(tourStatus1);
		report1.setTourStatus2(tourStatus2);

		Report report2 = baseMapper.selectPatrolType(report);
		report1.setPatrolType0(report2.getPatrolType0());
		report1.setPatrolType1(report2.getPatrolType1());
		report1.setPatrolType2(report2.getPatrolType2());
		return report1;
	}

	@Override
	public Report getMonthTourTotal(Report report) {

		Report report1 = new Report();
		//处理查询参数
		int startMonth = getMonth(report);
		DateTime endDate = DateUtil.parse(report.getEndTime(), ConstantUtil.SDFDATE);
		DateTime startDate = DateUtil.parse(report.getStartTime(), ConstantUtil.SDFDATE);
		Long count = DateUtil.betweenMonth(endDate, startDate, false) + 1;
		int betweenMonth = count.intValue();
		int endMonth = DateUtil.month(endDate) + 1;

		//初始化数组
		int[] monthTotal = new int[betweenMonth];
		String[] months = new String[betweenMonth];
		for (int i = 0; i < betweenMonth; i++) {
			monthTotal[i] = 0;
			int month = DateUtil.offsetMonth(startDate, i + 1).getField(DateField.MONTH);
			if (month == 0) {
				month = 12;
			}
			months[i] = month + "月";
		}
		List<Report> reportList = baseMapper.selectTourStatus(report);
		//一个月最多5周
		if (CollectionUtil.isNotEmpty(reportList)) {
			for (int i = 0; i < reportList.size(); i++) {
				Report info = reportList.get(i);
				int index;
				int infoMonth = info.getInfoMonth();
				if (infoMonth <= endMonth) {
					index = (betweenMonth - 1) + infoMonth - endMonth;
				} else {
					//如果大于结束月，说明是上一年
					index = infoMonth - startMonth;
				}
				monthTotal[index] = info.getTourTotal();
			}
		}
		report1.setMonths(months);
		report1.setMonthTourTotal(monthTotal);
		return report1;
	}

	/**
	 * 查询工单、巡检、灯杆
	 *
	 * @param report
	 * @return
	 */
	@Override
	public R getLightInfo(Report report) {
		//spliceTime(report);

		if (report.getAreaId() != null) {
			BaseArea area = baseAreaMapper.getById(report.getAreaId());
			if (area == null) {
				return R.ok();
			}
			report.setSubCode(area.getSubCode());
			report.setTreeLeavel(area.getTreeLeavel());
			report.setAreaId(area.getId());
			if (report.getAreaType().equals("2")) {
				report.setSubCodeLength(report.getSubCode().length());
			}
		} else {
			if (report.getAreaType().equals(NumericEnum.ONE.getStrCode())) {
				return R.ok();
			}
			BaseArea bo = new BaseArea();
			bo.setAreaType(report.getAreaType());
			List<BaseArea> areaList = baseAreaMapper.getPage(bo);
			List<Report> reports = new ArrayList<>();
			for (BaseArea area : areaList) {
				Report object = new Report();
				object.setSubCode(area.getSubCode());
				object.setTreeLeavel(area.getTreeLeavel());
				object.setAreaId(area.getId());
				object.setSubCodeLength(area.getSubCode().length());
				reports.add(object);
			}
			report.setReportList(reports);
		}



		//区域类型(1管控2专属) //flag=gd/xs
		if (report.getAreaType().equals("1") && report.getFlag().equals("gd")) {
			report.setSubCodeLength(report.getSubCode().length() + 4);
			report.setTreeLeavel(report.getTreeLeavel() + 1);
			List<Report> gkTaskInfCount = baseMapper.getGkTaskInfCount(report);
			return R.ok(gkTaskInfCount);
		}
		if (report.getAreaType().equals("1") && report.getFlag().equals("xs")) {
			report.setTreeLeavel(report.getTreeLeavel() + 1);
			report.setSubCodeLength(report.getSubCode().length() + 4);
			List<Report> gkTourCount = baseMapper.getGkTourCount(report);
			return R.ok(gkTourCount);
		}
		if (report.getAreaType().equals("2") && report.getFlag().equals("gd")) {
			List<Report> zsTaskInfCount = baseMapper.getZsTaskInfCount(report);
			return R.ok(zsTaskInfCount);
		}
		if (report.getAreaType().equals("2") && report.getFlag().equals("xs")) {
			List<Report> zsTourInfCount = baseMapper.getZsTourInfCount(report);
			return R.ok(zsTourInfCount);
		}

		return R.ok();
	}

	/**
	 * 大屏查询
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report selectPatrolTotal(Report report) {
		Report report1 = new Report();
		//spliceTime(report);

		tourPatrolTotal(report, report1);
		return report1;
	}

	/**
	 * 小程序
	 * 巡视任务统计
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report selectTourType(Report report) {
		Report report1 = new Report();

		handTime(report);
		tourPatrolTotal(report, report1);

		return report1;
	}

	/**
	 * 统计不同巡视类型下的不同状态巡视任务数量
	 *
	 * @param report
	 * @param report1
	 */
	private void tourPatrolTotal(Report report, Report report1) {
		Report report2 = baseMapper.selectTourType(report);
		if (report2 == null) {
			return;
		}
		int[] patrolTypeTotal = new int[3];
		patrolTypeTotal[0] = report2.getPatrolTypeZ0();
		patrolTypeTotal[1] = report2.getPatrolTypeZ1();
		patrolTypeTotal[2] = report2.getPatrolTypeZ2();
		int[] patrolStatusTotal0 = new int[3];
		patrolStatusTotal0[0] = report2.getPatrolTypeWxs0();
		patrolStatusTotal0[1] = report2.getPatrolTypeWxs1();
		patrolStatusTotal0[2] = report2.getPatrolTypeWxs2();

		//进行中
		int[] patrolStatusTotal1 = new int[3];
		patrolStatusTotal1[0] = report2.getPatrolType01();
		patrolStatusTotal1[1] = report2.getPatrolType11();
		patrolStatusTotal1[2] = report2.getPatrolType21();
		//已结束
		int[] patrolStatusTotal2 = new int[3];
		patrolStatusTotal2[0] = report2.getPatrolTypeEnd0();
		patrolStatusTotal2[1] = report2.getPatrolTypeEnd1();
		patrolStatusTotal2[2] = report2.getPatrolTypeEnd2();

		report1.setPatrolTypeTotal(patrolTypeTotal);
		report1.setPatrolStatusTotal0(patrolStatusTotal0);
		report1.setPatrolStatusTotal1(patrolStatusTotal1);
		report1.setPatrolStatusTotal2(patrolStatusTotal2);
	}

	/**
	 * 工单来源 与故障分布
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report selectFaultLevee(Report report) {
		handTime(report);
		return baseMapper.selrctUserTaskLogTotal(report);
	}

	/**
	 * 查询工单数与巡检数
	 *
	 * @param report
	 * @return
	 */
	@Override
	public Report selectTaskAndTour(Report report) {
		Report report1 = new Report();
		handTime(report);
		//查询个人工单数
		Report report3 = baseMapper.selrctUserTaskLogTotal(report);
		BeanUtils.copyProperties(report3, report1);
		int count = baseMapper.tourTaskDetailCount(report).intValue();
		//查询巡视数
		report1.setTourTaskDetailCount(count);
		return report1;
	}

	/**
	 * 微信退出登录
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public Object logOut(Long userId) {
		return baseMapper.miniLogOut(userId);
	}

	/**
	 * 统计故障分布故障数
 	 * @param report
	 * @return
	 */
	@Override
	public List<DeviceFaultVo> countDeviceFault(Report report) {
		spliceTime(report);
		List<DeviceFaultVo> deviceFaultVoList = baseMapper.getDeviceFault(report);
		R<List<SysDictItem>> serviceDictByType = remoteDictService.getDictByType("early_warning_type");
		List<DeviceFaultVo> deviceFaultVos = new ArrayList<>();
		if (CollectionUtil.isNotEmpty(serviceDictByType.getData())){
			List<SysDictItem> sysDictItems = serviceDictByType.getData();
			for (SysDictItem sysDictItem:sysDictItems){
				DeviceFaultVo deviceFaultVo = new DeviceFaultVo();
				deviceFaultVo.setFaultLevel(sysDictItem.getValue());
				Optional<DeviceFaultVo> deviceFaultVoOptional = deviceFaultVoList.stream().filter(n -> n.getFaultLevel().equals(sysDictItem.getValue())).findFirst();
				deviceFaultVoOptional.ifPresent(faultVo -> deviceFaultVo.setSumCount(faultVo.getSumCount()));
				deviceFaultVos.add(deviceFaultVo);
			}
		}
		return deviceFaultVos;
	}

	private void handTime(Report report) {
		if (CommonUtil.isNotBlank(report.getStartTime())) {
			DateTime date = DateUtil.parse(
					report.getStartTime(), "yyyy-MM");
			report.setStartTime(DateUtil.format(DateUtil.beginOfMonth(date), ConstantUtil.SDFDATETIME));
			DateTime dateTime1 = DateUtil.endOfMonth(date);
			report.setEndTime(DateUtil.format(dateTime1, ConstantUtil.SDFDATETIME));
		}

	}
}
